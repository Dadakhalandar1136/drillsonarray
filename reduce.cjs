function functionReduce(elements, cb, startingValue) {

    let arrayValue = 0;
    if (Array.isArray(elements) === false || elements.length === 0) {
        return [];
    }
    let index = 0;
    if (startingValue === undefined) {
        startingValue = elements[0];
        index = 1;
    }
    for (; index < elements.length; index++) {
        arrayValue = cb(startingValue, elements[index], index, elements);
        startingValue = arrayValue;
    }
    return arrayValue;
}

module.exports = functionReduce;