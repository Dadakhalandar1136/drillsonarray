function FunctionMap(items, cb) {

    let mapArray = [];
    if (Array.isArray(items) === false || items.length === 0) {
        return [];
    }
    let index = 0;
    for (index = 0; index < items.length; index++) {
        mapArray.push(cb(items[index], index, items));
    }
    return mapArray;

}

module.exports = FunctionMap;