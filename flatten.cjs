let flattendArray = [];
function functionForFlatten(element, depth) {

    if (element === undefined || element.length === 0) {
        return [];
    }
    if (depth == undefined) {
        depth = 1;
    }
    for (let index = 0; index < element.length; index++) {
        if (Array.isArray(element[index]) == true) {
            if (depth >= 1) {
                functionForFlatten(element[index], depth - 1);
            } else {
                flattendArray.push(element[index]);
            }
        } else {
            if (element[index] != undefined) {
                flattendArray.push(element[index]);
            }
        }
    }
    return flattendArray;
}

module.exports = functionForFlatten;