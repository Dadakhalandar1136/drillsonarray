const FunctionMap = require('../map.cjs');

const items = [1, 2, 3, 4, 5, 5];

let forReturn = function (element, index, items) {
    return element * items[index];
}

const arrayResult = FunctionMap(items, forReturn);
console.log(arrayResult);