const functionForFlatten = require('../flatten.cjs');

const nestedArray = [1, [2], [[3]], [[[4]]]];
let depth = 3;
const forReturn = functionForFlatten(nestedArray, depth);
console.log(forReturn);