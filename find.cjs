function functionFind(element, cb) {

    if (element === undefined || Array.isArray(element) === false || element.length === 0) {
        return [];
    }
    for (let index in element) {
        if (cb(element[index])){
            return element[index];
        }
    }
    return undefined;
}

module.exports = functionFind;