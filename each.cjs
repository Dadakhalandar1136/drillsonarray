function funcForEach(items, cb) {

    if (items === undefined || Array.isArray(items) === false || items.length === 0) {
        return [];
    } else {
        for (let element in items) {
            items[element] = cb(items[element], element);
        }
        return "forEach function using callback function."
    }
}

module.exports = funcForEach;