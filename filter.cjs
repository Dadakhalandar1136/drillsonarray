function functionFilter(element, cb) {

    let filterArray = [];
    if (element === undefined || Array.isArray(element) === false || element.length === 0) {
        return [];
    }
    for (let index in element) {
        if (cb(element[index], index, element) == true) {
            filterArray.push(element[index]);
        }
    }
    return filterArray;
}

module.exports = functionFilter;